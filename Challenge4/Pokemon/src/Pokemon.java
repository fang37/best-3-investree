public class Pokemon {
    String name;
    String element;
    float healthPoint;
    int attackPoint;

    Pokemon(String name, String element, float healthPoint, int attackPoint) {
        this.name = name;
        this.element = element;
        this.healthPoint = healthPoint;
        this.attackPoint = attackPoint;
    }

    public void attack() {
        System.out.println(name + "'s attack");
    }

    public void useItem() {
        System.out.println(name + "'s use item");
    }

    public void retreat() {
        System.out.println(name + "'s retreat");
    }
}
