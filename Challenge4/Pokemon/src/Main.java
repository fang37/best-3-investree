public class Main {
    public static void main(String[] args) {
        Pokemon pikachu = new Pokemon("Pikachu", "Electric", 100, 70);
        pikachu.attack();
        pikachu.useItem();
        pikachu.retreat();

    }
}