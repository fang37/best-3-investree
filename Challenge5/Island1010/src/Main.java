import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int choice = 0;
        System.out.println("Welcome to 10x10 Island!");

        Player player = new Player();

        while (choice != -1){
            System.out.println("Choose :");
            System.out.println("1. Move");
            System.out.println("2. Pull Over");
            System.out.println("3. Restart");
            System.out.println("-1. Exit");
            choice = scan.nextInt();

            switch (choice) {
                case 1:
                    player.move();
                    break;
                case 2:
                    player.pullOver();
                    break;
                case 3:
                    player.restart();
                default:
                    break;
            }
            player.printPosition();
        }



    }
}