import java.util.Scanner;

public class Player {
    int posX = 5;
    int posY = 6;
    char[] mapY = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};

    Player() {
        printPosition();
    }


    public void move() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Input direction N, W, E, S : ");
        char direction = scan.next().charAt(0);
        direction = Character.toUpperCase(direction);

        System.out.print("Input distance : ");
        int distance = scan.nextInt();;

        switch (direction) {
            case 'N':
                moveToN(distance);
                break;
            case 'W':
                moveToW(distance);
                break;
            case 'E':
                moveToE(distance);
                break;
            case 'S':
                moveToS(distance);
                break;
            default:
                System.out.println("Wrong direction");
        }
    }

    public void pullOver() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Input direction N, W, E, S : ");
        char direction = scan.next().charAt(0);
        direction = Character.toUpperCase(direction);

        switch (direction) {
            case 'N':
                posY = 1;
                break;
            case 'W':
                posX = 1;
                break;
            case 'E':
                posX = 10;
                break;
            case 'S':
                posY = 10;
                break;
            default:
                System.out.println("Wrong direction");
        }
    }

    public void restart() {
        posX = 5;
        posY = 6;
    }

    public void printPosition() {
        System.out.println("Current Position: "+getYPosition(posY)+posX) ;
    }

    private char getYPosition(int posY) {
        return mapY[posY-1];
    }

    private void moveToN(int distance) {
        posY -= distance;
        if (posY < 1) {
            posY = 1;
        }

    }

    private void moveToS(int distance) {
        posY += distance;
        if (posY > 10) {
            posY = 10;
        }
    }

    private void moveToW(int distance) {
        posX -= distance;
        if (posX < 1) {
            posX = 1;
        }
    }

    private void moveToE(int distance) {
        posX += distance;
        if (posX > 10) {
            posX = 10;
        }
    }
}
