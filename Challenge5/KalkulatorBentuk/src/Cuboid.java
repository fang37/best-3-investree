import java.util.Scanner;

public class Cuboid {
    float lengthX;
    float lengthY;
    float lengthZ;
    float volume;

    Cuboid(){
        inputData();
    }

    Cuboid(float lengthX, float lengthY, float lengthZ){
        this.lengthX = lengthX;
        this.lengthY = lengthY;
        this.lengthZ = lengthZ;
    }

    public void printVolume() {
        countVolume();
        System.out.println("Volume = "+volume);
    }

    private void countVolume() {
        volume = lengthX * lengthY * lengthZ;
    }
    private void inputData() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukan panjang X :");
        this.lengthX = scan.nextFloat();
        System.out.print("\n");
        System.out.print("Masukan panjang Y :");
        this.lengthY = scan.nextFloat();
        System.out.print("\n");
        System.out.print("Masukan panjang Z :");
        this.lengthZ = scan.nextFloat();
    }
}
