import java.util.Scanner;

public class Triangle {
    float length;
    float height;
    float area;

    Triangle(){
        inputData();
    }
    Triangle(float length, float height){
        this.length = length;
        this.height = height;
    }

    public void printArea() {
        countArea();
        System.out.println("Luas segitiga = "+area);
    }

    private void countArea() {
        area = (length * height)/2;
    }

    private void inputData() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukan panjang segitiga :");
        this.length = scan.nextFloat();
        System.out.print("\n");
        System.out.print("Masukan tinggi segitiga: ");
        this.height = scan.nextFloat();
    }
}
