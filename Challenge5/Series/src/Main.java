import java.util.Scanner;

public class Main {
    public static int[] a = new int[3];
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Masukan 3 bilangan awal");
        for (int i=0; i<a.length; i++) {
            System.out.print("a"+i+": ");
            a[i] = scan.nextInt();
        }

        int k = 0;

        while(k != -1){
            System.out.println("Cari nilai pada indeks ke: ");
            k = scan.nextInt();
            System.out.println(series(k));
        }
    }

    public static int series(int i) {
        if(i == 0) {return a[0];}
        if(i == 1) {return a[1];}
        if(i == 2) {return a[2];}
        if(i%3 == 0) {
            return series(i-3) + series(i-2);
        }
        if(i%3 == 1) {
            return series(i-2) + 2*series(i-1);
        }
        if(i%3 == 2) {
            return (series(i-3) / 2) + series(i-1);
        }
        return 0;
    }
}