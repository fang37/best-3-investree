public class Main {
    public static void main(String[] args) {
        int[] myArray = {2, 3, 1, 4, 4, 5, 6};
        int maxValue = 0;

        for (int i=0; i<=myArray.length-1; i++) {
            for (int j=0; j<=myArray.length-1; j++) {
                if (i == j) {continue;}
                if (myArray[i] == myArray[j]) {break;}
                if (j==myArray.length-1 && myArray[i] > maxValue) {
                    maxValue = myArray[i];
                }
            }
        }
        System.out.println("Max number: "+maxValue);
    }
}