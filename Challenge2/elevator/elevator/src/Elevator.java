import java.util.Scanner;

public class Elevator {
    private static Integer EXIT_CODE = -1;
    int minFloor;
    int maxFloor;
    int currentFloor;
    Scanner scan = new Scanner(System.in);

    public Elevator(int minFloor, int maxFloor){
        this.minFloor = minFloor;
        this.maxFloor = maxFloor;
        this.currentFloor = minFloor;
    }

    public void start() {
        int inputFloor = minFloor;
        while (inputFloor != EXIT_CODE) {
            System.out.print("Go to floor: ");
            inputFloor = scan.nextInt();

            if (inputFloor >= minFloor && inputFloor <= maxFloor) {
                moveTo(inputFloor);
            }
            else {
                System.out.println("Please enter the between " + minFloor +"-"+maxFloor);
            }

        }
    }

    private void moveTo(int inputFloor) {
        while(currentFloor != inputFloor) {
            System.out.println("Elevator at floor "+(currentFloor));
            if (currentFloor < inputFloor) {
                currentFloor++;
            }
            else if(currentFloor > inputFloor) {
                currentFloor--;
            }
        }
        System.out.println("Elevator stopped at "+currentFloor);
    }
}
