import enums.SpotifyMenu
import feature.CredentialFeature
import java.util.Scanner

// lazy-initialization

val scanner = Scanner(System.`in`)

private lateinit var credentialFeature: CredentialFeature

fun main() {
    setup()
    welcome()

}

fun welcome() {
    println()
    println()
    println("SELAMAT DATANG DI SPOTIFY APP")
    println("1. Login")
    println("2. Profile")
    print("Pilih menu: ")

    when (scanner.nextLine()) {
        SpotifyMenu.LOGIN.id -> {
            credentialFeature.login()
        }
        SpotifyMenu.PROFILE.id -> {
            println(SpotifyMenu.PROFILE.desc)
            credentialFeature.getProfile()
        }

        else -> println("Kode input tidak dapat di proses")
    }
    welcome()
}

fun setup() {
    credentialFeature = CredentialFeature(scanner)
}
