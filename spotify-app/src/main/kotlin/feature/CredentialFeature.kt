package feature

import database.UserDatabaseImpl
import model.UserData
import service.CredentialService
import service.CredentialServiceImpl
import java.util.Scanner

class CredentialFeature(private val scanner: Scanner) {

    private val credentialService: CredentialService = CredentialServiceImpl()

    fun login() {
        print("Username : ")
        val inputUsername = scanner.nextLine()

        print("Password : ")
        val inputPassword = scanner.nextLine()

        val loggedInUser = credentialService.doLogin(inputUsername, inputPassword)?.let {
            println("Username : ${it.username}")
            println("Nama : ${it.name}")
            println("Address : ${it.address}")
        }
    }

    fun getProfile() {
        credentialService.getLoggedInUser()?.let {
            printUserData(it)
        } ?: print("anda belum login")

    }

    private fun printUserData(userData: UserData) {
        println("Username : ${userData.username}")
        println("Nama : ${userData.name}")
        println("Address : ${userData.address}")
    }
}