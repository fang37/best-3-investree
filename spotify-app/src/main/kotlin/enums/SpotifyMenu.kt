package enums

enum class SpotifyMenu(val id: String, val desc: String) {
    LOGIN("1", "Gunakan username dan password untuk masuk aplikasi"),
    PROFILE("2", "Menunjukan informasi user yang sedang login")
}