package database

import model.UserData

interface UserDatabase {

    fun findAllUser(): List<UserData>

    fun findUser(username: String): UserData?

    fun addUser(data: UserData)

//    fun authUser( username: String, password: String): UserData?
}