package database

import model.UserData

class UserDatabaseImpl : UserDatabase {

    private val userList = mutableListOf(
        UserData(1, "Irfan Ghifari", "irfanghifari", "irfan123", "Cianjur"),
        UserData(2, "Ghifari Irfan", "ghifariirfan", "ghifari123", "Bandung")
    )

    override fun findAllUser(): List<UserData> = userList // { return userList }

    override fun findUser(username: String): UserData? = userList.find {
        it.username.equals(username, true)
    }

    override fun addUser(data: UserData) {
        userList.add(data)
    }


}